#! /bin/bash
set -x

docker stop eosio && docker rm eosio

docker run --name eosio \
  --publish 7777:7777 \
  --publish 127.0.0.1:5555:5555 \
  --volume $(pwd)/bancor_exchange:/contracts/bancor_exchange \
  --detach \
  eosio/eos:v1.4.0 \
  /bin/bash -c \
  "keosd --http-server-address=0.0.0.0:5555 & exec nodeos -e -p eosio --plugin eosio::producer_plugin --plugin eosio::chain_api_plugin --plugin eosio::history_plugin --plugin eosio::history_api_plugin --plugin eosio::http_plugin -d /mnt/dev/data --config-dir /mnt/dev/config --http-server-address=0.0.0.0:7777 --access-control-allow-origin=* --contracts-console --http-validate-host=false --filter-on='*' --verbose-http-errors"

sleep 10
docker logs --tail 10 eosio


shopt -s expand_aliases
source ./comspec.sh

cleos wallet create -f /tmp/my-wallet-passwd
docker exec eosio bash -c "cat /tmp/my-wallet-passwd" | sed -r 's/"(.*)"/\1/g' > ./my-wallet-passwd
cleos wallet open
cleos wallet unlock --password "$(cat ./my-wallet-passwd)"
cleos wallet create_key | sed -r 's/.*"(.*)".*/\1/g' > ./my-wallet-public-key
cleos wallet import --private-key 5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3

cleos create account eosio eosio.token EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV
cleos set contract eosio.token /contracts/eosio.token --abi eosio.token.abi -p eosio.token@active
cleos push action eosio.token create '[ "eosio", "1000000000.0000 BTC"]' -p eosio.token@active
cleos push action eosio.token create '[ "eosio", "1000000000.0000 ETH"]' -p eosio.token@active

cleos create account eosio exbtceth EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV -p eosio@active
cleos push action eosio.token issue '["exbtceth", "2000.0000 ETH", "hi"]' -p eosio@active
cleos push action eosio.token issue '["exbtceth", "1000.0000 BTC", "hi"]' -p eosio@active
cleos get currency balance eosio.token exbtceth ETH
cleos get currency balance eosio.token exbtceth BTC

cleos create account eosio geniuscirno EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV -p eosio@active
cleos push action eosio.token issue '["geniuscirno", "100.0000 ETH", "hi"]' -p eosio@active
cleos push action eosio.token issue '["geniuscirno", "100.0000 BTC", "hi"]' -p eosio@active
cleos get currency balance eosio.token exbtceth ETH
cleos get currency balance eosio.token exbtceth BTC

# cleos create account eosio bc.exchange EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV -p eosio@active
cleos set contract exbtceth /contracts/bancor_exchange -p exbtceth

cleos create account eosio exbtceth1 EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV -p eosio@active
cleos create account eosio exbtxethfee EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV -p eosio@active

cleos set account permission exbtceth active '{"threshold": 1,"keys": [{"key": "EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","weight": 1}],"accounts": [{"permission":{"actor":"exbtceth","permission":"eosio.code"},"weight":1}]}' owner -p exbtceth
cleos set account permission exbtceth1 active '{"threshold": 1,"keys": [{"key": "EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","weight": 1}],"accounts": [{"permission":{"actor":"exbtceth","permission":"eosio.code"},"weight":1}]}' owner -p exbtceth1

cleos push action exbtceth create '["exbtceth", "exbtceth1", "exbtcethfee", "BTCETH", "1000.0000 BTC", "2000.0000 ETH"]' -p exbtceth

cleos get table exbtceth exbtceth market

# cleos set account permission exbtceth active '{"threshold": 1,"keys": [{"key": "EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","weight": 1}],"accounts": []}' owner -p exbtceth

cleos set account permission geniuscirno active '{"threshold": 1,"keys": [{"key": "EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","weight": 1}],"accounts": [{"permission":{"actor":"exbtceth","permission":"eosio.code"},"weight":1}]}' owner -p geniuscirno
cleos push action exbtceth buytoken '["geniuscirno", "BTCETH", "10.0000 BTC"]' -p geniuscirno
# cleos set account permission exbtceth active '{"threshold": 1,"keys": [{"key": "EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","weight": 1}],"accounts": []}' owner -p geniuscirno
cleos get table exbtceth exbtceth market
cleos get currency balance eosio.token geniuscirno ETH
cleos get currency balance eosio.token geniuscirno BTC

cleos push action exbtceth selltoken '["geniuscirno", "BTCETH", "10.0000 ETH"]' -p geniuscirno
cleos get table exbtceth exbtceth market
cleos get currency balance eosio.token geniuscirno ETH
cleos get currency balance eosio.token geniuscirno BTC

cleos push action exbtceth addtoken '["geniuscirno", "BTCETH", "10.0000 BTC"]' -p geniuscirno
cleos get table exbtceth exbtceth market
cleos get currency balance eosio.token geniuscirno ETH
cleos get currency balance eosio.token geniuscirno BTC

cleos push action exbtceth subtoken '["geniuscirno", "BTCETH", "10.0000 BTC"]' -p geniuscirno
cleos get table exbtceth exbtceth market
cleos get currency balance eosio.token geniuscirno ETH
cleos get currency balance eosio.token geniuscirno BTC
