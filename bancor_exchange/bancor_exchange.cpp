#include <eosiolib/eosio.hpp>
#include <eosiolib/asset.hpp>
#include <eosiolib/print.hpp>

#include <string>
#include <cmath>

using namespace eosio;

using eosio::asset;
using eosio::symbol;

typedef double real_type;

const int64_t max_fee_rate = 10000;

class [[eosio::contract]] exchange : public eosio::contract {

    public:

        using contract::contract;

        exchange(name self, name code, datastream<const char*> ds) : contract(self, code, ds), _market(_self, _self.value) {}

        [[eosio::action]]
            void create(name payer, name exchange_account, name fee_account, std::string relay_symbol, asset quote_supply, asset base_supply) {
                require_auth(_self);

                eosio_assert(quote_supply.amount>0, "invalid quote_supply amount");
                print("quote_supply ",quote_supply);
                eosio_assert(quote_supply.symbol.is_valid(), "invalid quote_supply symbol");
                eosio_assert(base_supply.amount>0, "invalid base_supply amount");
                print("base_supply ", base_supply);
                eosio_assert(base_supply.symbol.is_valid(), "invalid base_supply symbol");

                auto supply_symbol = symbol(relay_symbol,4);

                print("supply_symbol ",supply_symbol);

                auto iter = _market.find(supply_symbol.raw());
                eosio_assert(iter==_market.end(), "market already created");

                action(
                        permission_level{payer, "active"_n},
                        "eosio.token"_n,
                        "transfer"_n,
                        std::make_tuple(payer, exchange_account, quote_supply, std::string("send quote_supply to exchange_account"))
                      ).send();

                action(
                        permission_level{payer, "active"_n},
                        "eosio.token"_n,
                        "transfer"_n,
                        std::make_tuple(payer, exchange_account, base_supply, std::string("send base_supply to exchange_account"))
                      ).send();

                _market.emplace(_self, [&](auto& m){
                        m.supply.amount = 100000000000000ll;
                        m.supply.symbol = supply_symbol;
                        m.base.balance.amount = base_supply.amount;
                        m.base.balance.symbol = base_supply.symbol;
                        m.quote.balance.amount = quote_supply.amount;
                        m.quote.balance.symbol = quote_supply.symbol;

                        m.exchange_account = exchange_account;
                        m.fee_account = fee_account;

                        m.shareholder.account[payer].base_in = base_supply;
                        m.shareholder.account[payer].quote_in = quote_supply;
                        m.shareholder.account[payer].base_holding = base_supply;
                        m.shareholder.account[payer].quote_holding = quote_supply;
                        m.shareholder.total_quote = quote_supply;
                        });
            }

        [[eosio::action]]
            void buytoken(name payer, std::string relay_symbol, asset quote_in) {
                require_auth(payer);

                eosio_assert(quote_in.amount >0 ,"quote_in amount must > 0");
                eosio_assert(quote_in.symbol.is_valid(), "invalid quote_in symbol type");

                auto supply_symbol = symbol(relay_symbol, 4);
                print("supply_symbol ", supply_symbol);

                auto iter = _market.find(supply_symbol.raw());
                eosio_assert(iter!=_market.end(), "market not exists");

                auto quote_after_fee = quote_in;
                auto quote_fee = asset{0, quote_in.symbol};
                auto fee_rate = iter->quote_fee;
                print("fee_rate ",fee_rate);
                if (fee_rate > 0) {
                    quote_fee = calc_fee(quote_in, fee_rate);

                    action(
                            permission_level{payer, "active"_n},
                            "eosio.token"_n,
                            "transfer"_n,
                            std::make_tuple(payer, iter->fee_account, quote_fee, std::string("send quote fee to fee_account"))
                          ).send();
                    quote_after_fee -= quote_fee;
                }
                print("quote_after_fee ",quote_after_fee);
                print("iter.quote ",iter->quote.balance);

                eosio_assert(quote_after_fee.amount > 0, "quote_after_fee.amount must > 0");

                action(
                        permission_level{payer, "active"_n},
                        "eosio.token"_n,
                        "transfer"_n,
                        std::make_tuple(payer, iter->exchange_account, quote_after_fee, std::string("send quote to exchange_account"))
                      ).send();


                auto base_in = asset{};
                _market.modify(iter, payer, [&](auto& es) {
                        base_in= es.convert(quote_after_fee, iter->base.balance.symbol);
                        es.quote.balance += quote_fee;
                        });

                action(
                        permission_level{iter->exchange_account, "active"_n},
                        "eosio.token"_n,
                        "transfer"_n,
                        std::make_tuple(iter->exchange_account, payer, base_in, std::string("receive base from exchange_account"))
                      ).send();

            }

        [[eosio::action]]
            void selltoken(name payer, std::string relay_symbol, asset base_in) {
                require_auth(payer);

                eosio_assert(base_in.amount >0 ,"base amount must > 0");
                eosio_assert(base_in.symbol.is_valid(), "invalid base_in symbol type");

                auto supply_symbol = symbol(relay_symbol, 4);
                print("supply_symbol ", supply_symbol);

                auto iter = _market.find(supply_symbol.raw());
                eosio_assert(iter!=_market.end(), "market not exists");

                auto base_after_fee = base_in;
                auto base_fee = asset{0, base_in.symbol};
                auto fee_rate = iter->base_fee;
                print("fee_rate ",fee_rate);
                if (fee_rate > 0) {
                    base_fee = calc_fee(base_in, fee_rate);

                    action(
                            permission_level{payer, "active"_n},
                            "eosio.token"_n,
                            "transfer"_n,
                            std::make_tuple(payer, iter->fee_account, base_fee, std::string("send base fee to fee_account"))
                          ).send();
                    base_after_fee -= base_fee;
                }
                print("base_after_fee ",base_after_fee);
                print("iter.base ",iter->base.balance);

                eosio_assert(base_after_fee.amount > 0, "base_after_fee.amount must > 0");


                action(
                        permission_level{payer, "active"_n},
                        "eosio.token"_n,
                        "transfer"_n,
                        std::make_tuple(payer, iter->exchange_account, base_after_fee, std::string("send base to exchange_account"))
                      ).send();

                auto quote_in = asset{};
                _market.modify(iter, payer, [&](auto& es) {
                        quote_in = es.convert(base_after_fee, iter->quote.balance.symbol);
                        es.base.balance += base_fee;
                        });

                action(
                        permission_level{iter->exchange_account, "active"_n},
                        "eosio.token"_n,
                        "transfer"_n,
                        std::make_tuple(iter->exchange_account, payer, quote_in, std::string("receive quote from exchange_account"))
                      ).send();
            }

        [[eosio::action]]
            void addtoken(name payer, std::string relay_symbol, asset quote_in) {
                require_auth(payer);

                eosio_assert(quote_in.amount>0, "quote_in.amount must > 0");

                auto supply_symbol = symbol(relay_symbol, 4);
                print("supply_symbol ", supply_symbol);

                auto iter = _market.find(supply_symbol.raw());
                eosio_assert(iter!=_market.end(), "market not exists");

                asset base_in = (iter->base.balance * quote_in.amount) / iter->quote.balance.amount;

                action(
                        permission_level{payer, "active"_n},
                        "eosio.token"_n,
                        "transfer"_n,
                        std::make_tuple(payer, iter->exchange_account, quote_in, std::string("send quote_in to exchange_account"))
                      ).send();

                action(
                        permission_level{payer, "active"_n},
                        "eosio.token"_n,
                        "transfer"_n,
                        std::make_tuple(payer, iter->exchange_account, base_in, std::string("send base_in to exchange_account"))
                      ).send();

                auto quote = iter->quote.balance;
                auto base = iter->base.balance;
                auto total_quote = iter->shareholder.total_quote + quote_in;

                std::map<name, exchange_state::_shareholder::_investment> shareholder_account= iter->shareholder.account;
                if (shareholder_account.find(payer) == shareholder_account.end()) {
                    shareholder_account[payer].base_in = base_in;
                    shareholder_account[payer].quote_in = quote_in;
                    shareholder_account[payer].base_holding = base_in;
                    shareholder_account[payer].quote_holding = quote_in;
                } else {
                    shareholder_account[payer].base_in += base_in;
                    shareholder_account[payer].quote_in += quote_in;
                    shareholder_account[payer].base_holding += base_in;
                    shareholder_account[payer].quote_holding += quote_in;
                }


                for (auto i = shareholder_account.begin(); i != shareholder_account.end(); i++) {
                    auto p = real_type(i->second.quote_holding.amount) / real_type(total_quote.amount);
                    print("shareholder ", name{i->first},"|",i->second.quote_holding,"|",total_quote,"|",p,"|",base);

                    i->second.base_holding.amount = (base.amount + base_in.amount) * p;
                    eosio_assert(i->second.base_holding.amount>0 && i->second.base_holding.amount<= base.amount, "base overflow");

                    i->second.quote_holding.amount = (quote.amount + quote_in.amount) * p;
                    eosio_assert(i->second.quote_holding.amount>0 && i->second.quote_holding.amount<= quote.amount, "quote overflow");
                }


                _market.modify(iter, payer, [&](auto& es) {
                        es.quote.balance += quote_in;
                        es.base.balance += base_in;
                        es.shareholder.total_quote += quote_in;
                        es.shareholder.account = shareholder_account;
                        });
            }

        [[eosio::action]]
            void subtoken(name payer, std::string relay_symbol, asset quote_out) {
                require_auth(payer);

                eosio_assert(quote_out.amount>0, "quote_out.amount must > 0");

                auto supply_symbol = symbol(relay_symbol, 4);
                print("supply_symbol ", supply_symbol);

                auto iter = _market.find(supply_symbol.raw());
                eosio_assert(iter!=_market.end(), "market not exists");

                auto quote = iter->quote.balance;
                auto base = iter->base.balance;
                auto total_quote = iter->shareholder.total_quote;

                std::map<name, exchange_state::_shareholder::_investment> shareholder_account = iter->shareholder.account;
                for (auto i = shareholder_account.begin(); i != shareholder_account.end(); i++) {
                    real_type p = real_type(i->second.quote_holding.amount) / real_type(total_quote.amount);
                    print("shareholder ", name{i->first},"|",i->second.quote_holding,"|",total_quote,"|",p,"|",quote);

                    i->second.quote_holding.amount = quote.amount * p;
                    eosio_assert(i->second.quote_holding.amount>0 && i->second.quote_holding.amount <= quote.amount, "overflow");

                    i->second.base_holding.amount = base.amount *p;
                    eosio_assert(i->second.base_holding.amount>0 && i->second.base_holding.amount<= base.amount, "overflow");
                }

                auto base_out = (base * quote_out.amount) / total_quote.amount;
                eosio_assert(base_out < base, "base_out must < market->base.balance");
                eosio_assert(base_out.amount > 0 , "base_out must > 0");
                eosio_assert(shareholder_account.find(payer) != shareholder_account.end(), "shareholder not exists");

                shareholder_account[payer].base_in -=  base_out;
                shareholder_account[payer].quote_in -= quote_out;
                shareholder_account[payer].base_holding -= base_out;
                shareholder_account[payer].quote_holding -= quote_out;
                total_quote -= quote_out;


                action(
                        permission_level{iter->exchange_account, "active"_n},
                        "eosio.token"_n,
                        "transfer"_n,
                        std::make_tuple(iter->exchange_account, payer, quote_out, std::string("send quote_out to shareholder"))
                      ).send();

                action(
                        permission_level{iter->exchange_account, "active"_n},
                        "eosio.token"_n,
                        "transfer"_n,
                        std::make_tuple(iter->exchange_account, payer, base_out, std::string("send base_out to shareholder"))
                      ).send();

                _market.modify(iter, payer, [&](auto& es) {
                        es.quote.balance -= quote_out;
                        es.base.balance -= base_out;
                        es.shareholder.total_quote -= quote_out;
                        es.shareholder.account = shareholder_account;
               });
            }

        asset calc_fee(asset token, uint64_t fee_rate) {
            asset fee = token * fee_rate / max_fee_rate;
            if (fee.amount < 1) {
                fee.amount = 1;
            }
            fee.symbol = token.symbol;
            return fee;
        }
    private:
        class [[eosio::table]] exchange_state {
            public:
                asset supply;

                struct connector {
                    asset balance;
                    double weight = .5;
                    EOSLIB_SERIALIZE(connector, (balance)(weight));
                };


                connector base;
                connector quote;

                name exchange_account;
                name fee_account;
                uint64_t base_fee;
                uint64_t quote_fee;


                struct _shareholder {
                    struct _investment {
                        asset quote_in;
                        asset base_in;
                        asset quote_holding;
                        asset base_holding;
                    };
                    asset total_quote;
                    std::map<name, _investment> account;
                };
                _shareholder shareholder;

                uint64_t primary_key() const { return supply.symbol.raw();}

                asset convert_to_exchange(connector& c, asset in) {
                    real_type R(supply.amount);
                    real_type C(c.balance.amount+in.amount);
                    real_type T(in.amount);
                    real_type F(c.weight/1000.0); 
                    real_type ONE(1.0);

                    real_type E = -R *(ONE - std::pow(ONE + T/C, F));

                    int64_t issued = int64_t(E);

                    supply.amount += issued;
                    c.balance.amount += in.amount;

                    return asset(issued, supply.symbol);
                }
                asset convert_from_exchange(connector& c, asset in) {
                    eosio_assert(in.symbol == supply.symbol, "unexpected asset symbol input");

                    real_type R(supply.amount-in.amount);
                    real_type C(c.balance.amount);
                    real_type F(1000.0/c.weight);
                    real_type E(in.amount);
                    real_type ONE(1.0);

                    real_type T = C * (std::pow(ONE + E/R, F) - ONE);

                    int64_t out = int64_t(T);

                    supply.amount -= in.amount;
                    c.balance.amount -= out;

                    return asset(out, c.balance.symbol);
                }
                asset convert(asset from, symbol to) {
                    auto sell_symbol = from.symbol;
                    auto ex_symbol = supply.symbol;
                    auto base_symbol = base.balance.symbol;
                    auto quote_symbol = quote.balance.symbol;

                    if (sell_symbol != ex_symbol) {
                        if (sell_symbol == base_symbol) {
                            from = convert_to_exchange(base, from);
                        } else if (sell_symbol == quote_symbol) {
                            from = convert_to_exchange(quote, from);
                        } else {
                            eosio_assert(false, "invalid sell");
                        }
                    } else {
                        if (to == base_symbol) {
                            from = convert_from_exchange(base, from);
                        } else if (to == quote_symbol ) {
                            from = convert_from_exchange(quote, from);
                        } else {
                            eosio_assert(false, "invalid conversion");
                        }
                    }

                    if (to != from.symbol) {
                        return convert(from, to);
                    }
                    return from;
                }

                //EOSLIB_SERIALIZE (exchange_state, (supply)(base)(quote)(exchange_account))
        };

        typedef eosio::multi_index<"market"_n, exchange_state> market;
        market _market;

};
EOSIO_DISPATCH( exchange, (create)(buytoken)(selltoken)(addtoken)(subtoken))
