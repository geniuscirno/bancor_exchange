#! /bin/bash

workspace=$(cd "$1"; pwd)
cd "$workspace/docker/eosio.cdt"
docker build . -t eosio/eosio-cdt
cd "$workspace"

bash build.sh
bash start.sh


