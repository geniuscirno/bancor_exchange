# /bin/bash

docker run --rm -i \
     -v "$(pwd)/bancor_exchange:/contracts/bancor_exchange" \
      eosio/eosio-cdt  bash << EOF
         cd /contracts/bancor_exchange
         rm -r ./CMakeFiles ./CMakeCache.txt
         cmake . && make
         eosio-abigen bancor_exchange.cpp --contract=exchange --output=bancor_exchange.abi 
EOF
